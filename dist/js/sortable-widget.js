function onEndHandle(evt) {

    var container = $(evt.target);
    var sortUrl = container.data('sortable-url');

    if (sortUrl !== undefined) {
        var item = $(evt.item);

        var oldPosition = parseInt(item.data('sortable-order'));
        var newPosition = oldPosition + evt.newIndex - evt.oldIndex;

        $.post(sortUrl,
            {
                'id': item.data('sortable-id'),
                'position': newPosition
            },
            function (data) {
                var elements = container;
                container.children().each(function (k, v) {
                    $(v).data('order', k + 1);
                });
            }
        );
    }
}
