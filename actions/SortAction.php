<?php

namespace mkiselev\sortable\actions;


use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii2tech\ar\position\PositionBehavior;

class SortAction extends Action
{
    /**
     * @var yii\db\Query|array yii\db\Query OR array to create query object
     */
    public $modelFindQuery;

    /** @var string AR class name */
    public $modelClassName;

    /** @var yii\db\Query query object which will be used to find model */
    private $_query;


    public function init()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (isset($this->modelFindQuery) && isset($this->modelClassName)) {
            throw new InvalidConfigException('You must set just one field `modelFindQuery` OR `modelClassName`');
        } elseif (isset($this->modelFindQuery)) {

            if ($this->modelFindQuery instanceof Query) {
                $this->_query = $this->modelFindQuery;
            } else {
                $this->_query = Yii::createObject($this->modelFindQuery);
            }

        } elseif (isset($this->modelClassName)) {
            /** @var \yii\db\ActiveRecord $className */
            $className = $this->modelClassName;
            $this->_query = $className::find();
        } else {
            throw new InvalidConfigException('You must set `modelFindQuery` OR `modelClassName` field');
        }

        parent::init();
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function run()
    {
        $id = Yii::$app->request->post('id');
        $position = Yii::$app->request->post('position');

        /** @var \yii\db\ActiveRecord|null $model */
        $model = $this->_query->andWhere(['id' => $id])->one();

        if (empty($model)) {
            return [
                'error' => Yii::t('app', 'Model not found.'),
            ];
        }

        if (!$model instanceof ActiveRecord) {
            return [
                'error' => Yii::t('app', 'Model must inherit ActiveRecord class.'),
            ];
        }

        if (!$model->hasMethod('moveToPosition', true)) {
            return [
                'error' => Yii::t('app', 'Model must implement `moveToPosition` method.'),
            ];
        }

        /** @var PositionBehavior $model */
        $result = $model->moveToPosition(intval($position));

        if (!$result) {
            return [
                'error' => Yii::t('app', 'Can\'t move to position.'),
            ];
        }

        return [];
    }
}
