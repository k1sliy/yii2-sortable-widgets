<?php

namespace mkiselev\sortable\grid;


use mkiselev\sortable\widgets\SortableWidget;
use yii\grid\Column;
use yii\helpers\ArrayHelper;

class SortableColumn extends Column
{
    public $contentOptions = [
        'style' => 'width: 30px; cursor: pointer;',
    ];

    /** @var string|\Closure */
    public $content = '&#9776;';

    public $pluginOptions = [];

    public $orderAttribute = 'order';

    public $sortAction;


    public function init()
    {
        $gridRowOptions = ($this->grid->rowOptions instanceof \Closure) ? clone $this->grid->rowOptions : $this->grid->rowOptions;

        $this->grid->rowOptions = function ($model, $key, $index, $grid) use ($gridRowOptions) {
            if ($gridRowOptions instanceof \Closure) {
                $baseOptions = call_user_func($gridRowOptions, $model, $key, $index, $grid);
            } else {
                $baseOptions = $gridRowOptions;
            }

            return array_merge($baseOptions, [
                'data-sortable-id' => ArrayHelper::getValue($model, 'id'),
                'data-sortable-order' => ArrayHelper::getValue($model, $this->orderAttribute),
            ]);
        };

        $gridViewId = $this->grid->id;
        SortableWidget::widget([
            'sortAction' => $this->sortAction,
            'containerSelector' => "#{$gridViewId} tbody",
        ]);
    }

    protected function renderDataCellContent($model, $key, $index)
    {
        return is_callable($this->content) ? call_user_func($this->content, $model, $key, $index) : $this->content;
    }
}
