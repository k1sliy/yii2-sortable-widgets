This extension is a Yii2 wrapper for [RubaXa/Sortable](https://github.com/RubaXa/Sortable)

##### NOTICE (? TODO дополнить/решить проблему)
If you wanna add PositionBehavior to exist AR model, please be sure what you doesn't have empty or null values.

#### Configure

Add sort action to your controller:
```php
public function actions()
{
    return [
        'sort' => [
            'class' => mkiselev\sortable\actions\SortAction::class,
            'modelClassName' => Product::class,
        ],
    ];
}
```


This extension used [ActiveRecord Position Extension](https://github.com/yii2tech/ar-position)
and automaticly will be load on install.

Configure PositionBehavior in your class like this:
```php
public function behaviors()
{
    return [
        'positionBehavior' => [
            'class' => yii2tech\ar\position\PositionBehavior::class,
            
            /**
            * @var string attribute name, which will store position value.
            * This attribute should be an integer.
            */
            'positionAttribute' => 'sort',
            
            /**
            * @var array list of owner attribute names, which values split records into the groups,
            * which should have their own positioning.
            * Example: `['group_id', 'category_id']`
            */
            'groupAttributes' => ['category_id'],
        ],
    ];
}
```

#### Usage

With GridView as SortableColumn:
```php
$dataProvider = ActiveDataProvider([
    'query' => Product::find()->orderBy(['sort' => SORT_ASC]),
]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => mkiselev\sortable\grid\SortableColumn::class,
            'orderAttribute' => 'sort',
            'sortableAction' => ['product/sort'],
        ],
        // Any columns...
    ],
]);
```

Alone:
```php
mkiselev\sortable\widgets\SortableWidget::widget([
    'id' => 'groups-wrapper',
    'sortableAction' => ['group/sort'],
    'pluginOptions' => [
        'handle' => '.sort-icon',
    ],
]);
```

### Thanks

* [Lebedev Konstantin (@RubaXa)](https://github.com/RubaXa) for [RubaXa/Sortable](https://github.com/RubaXa/Sortable)
* [Paul Klimov (@klimov-paul)](https://github.com/klimov-paul) for [yii2tech/ar-position](https://github.com/yii2tech/ar-position)

Created by inspiration of:

* [Dmitrii Bondarenko (@himiklab)](https://github.com/himiklab) for [himiklab/yii2-sortable-grid-view-widget](https://github.com/himiklab/yii2-sortable-grid-view-widget)
* [Constantine Chuprik (@kotchuprik)](https://github.com/kotchuprik) for [kotchuprik/yii2-sortable-widgets](https://github.com/kotchuprik/yii2-sortable-widgets)