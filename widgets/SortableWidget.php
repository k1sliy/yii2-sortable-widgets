<?php

namespace mkiselev\sortable\widgets;


use mkiselev\sortable\assets\RubaxaSortableAsset;
use mkiselev\sortable\assets\SortableWidgetAsset;
use yii\base\Widget;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

class SortableWidget extends Widget
{
    /** @var string|array AJAX Action который будет вызван после сортировки */
    public $sortAction = ['sort'];

    /** @var string Селектор контейнера содержащего элементы */
    public $containerSelector;

    /** @var string Drag handle selector within list items */
    public $handle;

    /** @var JsExpression Element dragging ended */
    public $onEnd;

    public function init()
    {
        parent::init();

        if (is_null($this->onEnd)) {
            $this->onEnd = new JsExpression("onEndHandle");
        }

        $sortUrl = isset($this->sortAction) ? Url::to($this->sortAction) : null;

        $pluginOptions = Json::encode([
            'onEnd' => $this->onEnd,
            'handle' => $this->handle,
        ]);

        $view = $this->getView();
        RubaxaSortableAsset::register($view);
        SortableWidgetAsset::register($view);
        $view->registerJs("Sortable.create(document.querySelector('{$this->containerSelector}'), {$pluginOptions});", View::POS_READY);

        if (!is_null($sortUrl)) {
            $view->registerJs("document.querySelector('{$this->containerSelector}').setAttribute('data-sortable-url', '{$sortUrl}');", View::POS_READY);
        }
    }
}
