<?php

namespace mkiselev\sortable\assets;


use yii\web\AssetBundle;

class SortableWidgetAsset extends AssetBundle
{

    public $sourcePath = '@vendor/mkiselev/yii2-sortable-widgets/dist';

    public $js = [
        'js/sortable-widget.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
