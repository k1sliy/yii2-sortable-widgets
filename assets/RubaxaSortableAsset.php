<?php

namespace mkiselev\sortable\assets;


use yii\web\AssetBundle;

class RubaxaSortableAsset extends AssetBundle
{
    public $sourcePath = '@npm/sortablejs';

    public $js = [
        YII_DEBUG ? 'Sortable.js' : 'Sortable.min.js',
    ];
}
